# MicroTransitions Full Scale 2021W04

Experimenting on reporting the complete data collection, analysis, and
presentation for _one of the_ sampling events for the MicroTransitions
full-scale trial.

---

## Project status

- **2021-11-15 Mon**: Working on the specific methanogenic activity (SMA) data analysis.

## Reflection
## Authors

 - Marco Prevedello <m.prevedello1@nuigalway.ie>; [Preve92](https://twitter.com/Preve92)

## Version history

- **0.1.0**: You have to start somewhre.
- **0.2.0**: Giving structure to the project.

## Acknowledgments
## License
## Bibliography
